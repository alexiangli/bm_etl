---
title: "安正ETL命令行调度平台使用指南"
author: "lixiang <alexiangli@outlook.com>"
date: 2017-09-13 14:32:24
updated:
---

# 安正ETL命令行调度平台使用指南

该调度平台原来由孙总(孙承佼)开发, 后来由李想重新修改. 核心功能和逻辑沿用孙总的源代码. 该调度平台使用bash脚本语言开发, 所以需要依赖bash开发环境. 对于其他shell脚本语言, 并不保证兼容. 源代码在AIX系统上开发, 新开发的调度平台在CentOS 7.3 上开发, bash版本为4.2.46.

# 安装配置

## 安装SQL*PLUS

示例为CentOS系统下安装配置过程

从[下载连接](http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html)下载特定版本的以下文件:

* oracle-instantclient<version>-basic-xx.x.x.x.x-x.x86_64.rpm
* oracle-instantclient<version>-sqlplus-xx.x.x.x.x-x.x86_64.rpm

然后安装:

``` shell
sudo rpm -ivh *.rpm
```

最后配置环境:

``` shell
cat<<EOF>>$HOME/.bashrc
export ORACLE_HOME=/usr/lib/oracle/<version>/client64
export PATH=$ORACLE_HOME/bin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
EOF

source $HOME/.bashrc
```

## 配置 ETL调度平台

配置 `$ETLHOME` 变量, 指定调度平台根目录:

``` shell
echo "export ETLHOME=/path/to/etlhome" >> $HOME/.bashrc
source $HOME/.bashrc
```

添加可执行权限

``` shell
chmod +x $ETLHOME/start_etl.sh
```

在`$HOME`目录下, 创建调度依赖环境设置文件 `$HOME/.envset`, 内容包括:

``` shell
## 环境变量示例
# 任务根路径
export TASKPATH=$ETLHOME/test
# 数据库主机IP:端口号PORT, 例如: 192.168.1.1:1521
export DBHOST=
# 数据库名, 例如: ORCL
export DBNAME=
# 用户名
export DBUSER=
# 用户密码
export DBPWD=
```

创建任务目录, 并创建作业序列文件和脚本, 目录结构为:

```
test
|-- job  # 存放作业序列文件
|   |-- js_100.def  # 作业序列文件以 `.def` 结尾
|   |-- js_101.def
|   |-- js_102.def
|   |-- js_103.def
|   |-- js_104.def
|   |-- js_105.def
|   `-- js.rel  # 作业关系文件以 `js.rel` 命名
|-- log  # 存放调度日志, 若不存在, 程序将自动创建
`-- script  # 存放作业序列文件中指定的脚本文件
    |-- sql_01.sql  # sql脚本文件
    |-- sql_02.sql
    `-- sql_03.sql
```

运行脚本根据文本提示开始作业调度

``` shell
$ETLHOME/start_etl.sh
```

# 功能介绍

## 1. 启动作业调度

有两种方式启动作业调度, 从头执行和从特定作业序列文件执行. 从头执行将自动从作业序列关系文件中读取前续作业的第一个作业并执行. 从特定作业序列文件执行需手动指定作业序列名和序号.

## 2. 重启作业调度

当作业执行失败, 程序会创建一个重启标识文件, 重启标识文件中写入了失败的作业序列名和序号. 调度平台可以在此重启作业. 如果重启标识文件不存在, 则说明作业均执行成功, 无需再进行重启.

## 3. 监控作业调度状态

可以通过监控来查看作业调度和执行状态.

## 4. 终止作业调度

终止作业调度功能用于终止作业调度. 可以立即终止下一个作业, 或者手动指定终止作业序列名和序号. 终止作业后可以重新启动作业调度.

## 5. 重置作业调度

重置作业调度将删除运行标识和终止标识文件. 这样便可以启动新的作业调度.

# 调度文件

## 作业序列文件

### 1. 文件名规则

作业序列文件以 `js_xxx.def` 规则命名. 序号并不重要, 但是方便告知其执行的先后顺序, 先执行的序列文件的编号应该小于后执行的序列文件. 例如: `js_100.def` 应该先于 `js_101.def` 执行, `js_101.def` 先于 `js_200.def` 执行.

### 2. 作业序列定义规则

作业序列用来指定单一作业中的子作业序列. 在作业序列文件中(以 `js_100.def`)为例, 作业序列定义规则为:

```
<jobid>:<scriptfile>:<parameters>:<jobtype>
```

例如:

```
1:sql_01.sql:test:sqlplus
2:sql_02.sql:test:sqlplus
```

其中,

* `jobid` 为作业序号, 1~n依次递增
* `scriptfile` 为作业脚本
* `parameters` 为传入参数, 可以为空
* `jobtype` 为作业类型, 目前仅支持 `sqlplus` 类型.

## 作业序列关系文件

作业序列关系文件用于定义作业序列之间的依赖和触发关系, 包含了调用作业序列文件的顺序的信息. 作业序列关系文件命名为: `js.rel`. 关系规则如下:

```
<关系名>:<前续作业序列>:<后续作业序列>
```

其中,

* 关系名定义为 `relation` 与序号如 `10` 连接的字符串
* 前续作业序列与后续作业序列由 `:` 分隔
* 两个及以上前续作业序列由 `+` 分隔
* 两个及以上后续作业序列由 `|` 分隔

例如:

```
relation10:js_100.def+js_101.def:js_102.def
relation11:js_102.def:js_103.def|js_104.def|js_105.def
relation12:js_103.def+js_104.def+js_105.def:js_106.def
```

以上示例包含三个序列关系, 每个序列关系包含前续和后续作业. 前续作业全部成功完成将触发后续作业. 上一个序列关系的后续作业即为下一个序列关系的前续作业, 由此形成一个作业依赖链. 具体来说, `relation10` 中, `js_100.def` 和 `js_101.def` 的都成功执行完成将触发 `js_102.def`. `js_102.def` 的成功执行完成又将触发 `js_103.def`, `js_104.def`, `js_105.def`. 以此类推.

借助该关系文件, 可以巧妙地进行断点启动或者重新启动失败的作业. 断点执行和重新启动作业请阅读功能介绍部分.

## 作业脚本文件

在作业序列文件中, 指定了作业将执行的作业脚本. 脚本文件放置于 `$TASKPATH/job` 目录下.

# 日志文件

## 运行日志



## 调度日志
